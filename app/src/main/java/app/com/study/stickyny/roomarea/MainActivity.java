package app.com.study.stickyny.roomarea;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    private TextView mResultText;
    private static final double TRANS_VALUE = 0.09290304;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final EditText lengthEdit = (EditText) findViewById(R.id.length_edit);
        final EditText widthEdit = (EditText) findViewById(R.id.width_edit);
        mResultText = (TextView) findViewById(R.id.result_text);

        TextWatcher textWatcher = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                String lengthStr = lengthEdit.getText().toString();
                String widthStr = widthEdit.getText().toString();

                if (isFillOut(lengthStr, widthStr)) {
                    print(Double.parseDouble(lengthStr), Double.parseDouble(widthStr));
                } else {
                    Toast.makeText(MainActivity.this, getString(R.string.error_msg_empty), Toast.LENGTH_SHORT).show();
                }
            }
        };
        lengthEdit.addTextChangedListener(textWatcher);
        widthEdit.addTextChangedListener(textWatcher);
    }
    private boolean isFillOut(String edit1, String edit2) {
        if (edit1.trim().isEmpty() || edit2.trim().isEmpty()) {
            return false;
        } else {
            return true;
        }
    }

    private void print(double length, double width) {
        mResultText.setText(getString(R.string.result_text, areaCalc(length, width), areaCalc(toMeters(length), toMeters(width))));
    }

    private double toMeters(double value) {
        return Math.sqrt(((value * value) * TRANS_VALUE));
    }

    private double areaCalc(double length, double width) {
        return length * width;
    }
}
